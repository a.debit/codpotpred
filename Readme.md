# codPotPred

This repository includes the scripts used to build the Shiny application used to identify the transcriptomic content of the species included in the lncPlankton V1.0 database, a catalogue of marine plankton lncRNAs. 

## Description

codPotPred is a user-friendly Shiny tool for the identification/computation of lncRNAs the marine plankton species included in LncPlankton. Such transcripts can be predicted using ten tools: CPC2 ,CPAT, LGC, LncADeep, LncDC, lncFinder, longdist, mRNN, RNAmining, RNASamba.
We have also included a special case in which lncRNA can be identified using a majority voting procedure. Users can choose between a total of 11 tools and setting the filters: transcript length, peptide length, and whether or not to check the reverse strand. For majority 
voting, users can also set a threshold of the confidence score of lncRNAs. 

The tool displays the results as a stacked bar plot, stacked polar plot, or a table when lncRNAs were calculated for multiple species of the same phylum. It displays a piechart for a single species.  

![Screenshot](Screenshot.png "Screenshot")
 
## Prerequisites

Before to start, it is necessary to install all the following R libraries:
shiny, shinyWidgets, reshape2, wesanderson, dplyr, ggplot2, ggthemes, tidyverse,
ggrepel, shinybusy, shinyjs, DT, pbapply, plotly, leaflet, leaflet.minicharts, 
leaflet.extras, RMySQL, shinylogs

Setup the lncPankton database:
The relational schema of the lncPlankton database must be created with the following tables: phylum, species, transcript, and codpotclasses
The database can be populated using the csv file downloadable from the web server  https://www.lncplankton.bio.ens.psl.eu/download.php

## Principle of the majority voting procedure

![Pipeline_maj_voting](pipeline_maj_voting.png "Majority Voting for lncRNA identification")
	
	
## Running the app

To use this tool on local machine, simply clone this repository on your machine by:

```
git clone https://gitlab.com/a.debit/codpotpred.git
```
And the launch the app
```
cd <app_folder>
runApp("codPotPred")
```

# Publication

Debit, A., Vincens, P., Bowler, C. and Cruz de Carvalho, H., 2023. LncPlankton V1. 0: a comprehensive collection of plankton long non-coding RNAs. bioRxiv, pp.2023-11.

# Authors

* **Ahmed Debit** - Post-Doc Bioinformatics IBENS Paris
 